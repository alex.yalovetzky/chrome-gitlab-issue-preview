# Gitlab Board Preview
El problema es que en los boards de gitlab no hay preview del issue. Si clickeas en la tarjeta se abre un sidebar donde no aparece la descripción de la tarea. Es molesto tener que clickear en el titulo y abrir en una nueva pestaña el detalle. Esta extensión resuelve el problema, al clickear en la tarjeta (no en el título) el usuario ahora va a ver un popup con el detalle del issue.

![Preview](preview.png)

# Instalación
* Primero clonar el repo en alguna carpeta local.
* En chrome ir a esta dirección chrome://extensions/
* Habilitar "Developer Mode" (arriba a la derecha)
* Usar la opción "Load Unpacked"
* Indicar la carpeta donde está el source del repo
* Asegurarse que el plugín quedó habilitado con el nombre "Etermax Gitlab Issue Preview"
* Si está bien habilitado deberias ver el ícono de gitlab en donde se muestran las extensiones de chrome