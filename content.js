var modal = `<div class="et-modal">
  {replace}
</div>`;

var addListeners = function(){
	var theHost = 'https://gitlab.com';
	$('body').on('click', 'li.board-card', function(){        
        var url = theHost + $(this).find('a').attr('href');
        $.get(url, function(data) {
        	var testDOM = $('<div/>').html(data).contents();
    		var desc = testDOM.find('.detail-page-description').html();

        	var modalContent = '<div class="detail-page-description content-block">' + desc + '</div>';
        	var content = modal.replace('{replace}', modalContent);
        	$(content).appendTo('body').modal();

        	$('.et-modal').removeClass('modal');
        });

    });
}

$(function() {
	addListeners();	
});